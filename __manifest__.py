# -*- coding: utf-8 -*-
{
    'name': "gicsales",

    'summary': """
        Sales yur products""",

    'description': """
        Sales
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'point_of_sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/produits.xml',
        'views/produits_wizard.xml',
        
    ],
    # only loaded in demonstration mode
    'demo': [
       
    ],
}